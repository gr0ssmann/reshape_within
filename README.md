# reshape\_within

Easily and extensibly reshape "within" data.

## License

All works contained in this repository are under the [CC0](https://creativecommons.org/publicdomain/zero/1.0/) license, version 1.0 or (at your option) any later version.
