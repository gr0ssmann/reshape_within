import pandas as pd

data = pd.read_csv("data.csv")


def reshape_within(data, subject, treatment, outcomes):
    m = sum(treatment)
    assert m == sum(outcomes)

    out = pd.DataFrame(columns=["subject", "treat", "y"])

    for j in range(data.shape[0]):
        temp_df = pd.DataFrame(
            {
                "subject": data.loc[j, subject],
                "treat": data.loc[j, treatment].tolist(),
                "y": data.loc[j, outcomes].tolist(),
            }
        )
        out = out.append(temp_df)

    return out


data2 = reshape_within(
    data,
    subject="subject",
    treatment=data.columns.str.startswith("treat"),
    outcomes=data.columns.str.startswith("y"),
)
